var readability = require("./index");
var Readability = readability.Readability;
var JSDOMParser = readability.JSDOMParser;
var path = require("path");
var fs = require("fs");
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json({limit: '50mb'}));

var port = 54312; // set our port

var router = express.Router(); // get an instance of the express Router

router.route('/readability')

.post(function(req, res) {

    var uri = {
        spec: "http://fakehost/test/page.html",
        host: "fakehost",
        prePath: "http://fakehost",
        scheme: "http",
        pathBase: "http://fakehost/test/"
    };


    var doc = new JSDOMParser().parse(req.body.content);

    var readability = new Readability(uri, doc);

    res.json({
        content: readability.parse().content.replace(/<(?:.|\n)*?>/gm, ' ')
    });


});

app.use('/api', router);

app.listen(port);
